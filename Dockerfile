FROM nginx:1.21.1
LABEL maintainer="Ulrich MONJI"

# Install dependencies
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Remove the default Nginx content
RUN rm -Rf /usr/share/nginx/html/*

# Copy the project content into the Nginx server
COPY . /usr/share/nginx/html

# Optionally customize nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]
